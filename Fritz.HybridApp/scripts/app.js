(function (global) {

  // TODO: Configure this URL to point to your project
  //var apiUrl = "http://MYWEBSITE/api/Order";

  var app = global.app = global.app || {};

  var getOrders = function (viewModel) {
    global.app.application.showLoading();
    $.get(apiUrl, function (data) {
      viewModel.set("orders", data);
      global.app.application.hideLoading();
    });
  };

  var OrdersService = kendo.Class.extend({
    init: function () {

      // A reference to the base class object
      var that = this;

      // Create the viewModel
      that.viewModel = kendo.observable({

        orders: [],
        refresh: function () {
          getOrders(this);
        },

        // Expose navigation
        orderSelected: function (e) {

          if (that.detailViewModel) {

            that.detailViewModel.set("details", e.dataItem.GamesOrdered);
            that.detailViewModel.set("itemCount", e.dataItem.ItemCount);

          } else {

            that.detailViewModel = new kendo.observable({
              details: e.dataItem.GamesOrdered,
              itemCount: e.dataItem.ItemCount,
              back: function () {
                global.app.application.navigate("#:back");
              }

            });

          }

          global.app.application.navigate("#orderDetails");
        }

      });


    }

  });

  // Define the OrderService
  global.app.orderService = new OrdersService();

  document.addEventListener('deviceready', function () {

    // TODO: Wire this up to launch in the correct order
    app.application = new kendo.mobile.Application(document.body, {
      layout: "main", init: function () {

        global.app.application = this;
        getOrders(global.app.orderService.viewModel);
      }
    });

  }, false);



})(window);