﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Fritz.SampleStore.Models;

namespace Fritz.SampleStore.Controllers
{
    public class OrderController : ApiController
    {

    private OrderRepository _Repo;

    public OrderController()
    {
      this._Repo = new OrderRepository();
    }

        // GET: api/Order
        public IEnumerable<Order> Get()
        {
          return _Repo.Get(_ => true);
        }

        // GET: api/Order/5
        public Order Get(int id)
        {
            return _Repo.GetById(id);
        }

        // POST: api/Order
        public void Post([FromBody]Order value)
        {
          _Repo.Add(value);
        }

        // PUT: api/Order/5
        public void Put(int id, [FromBody]Order value)
        {
          _Repo.Update(value);
        }

        // DELETE: api/Order/5
        public void Delete(int id)
        {
          _Repo.Delete(id);
        }
    }
}
