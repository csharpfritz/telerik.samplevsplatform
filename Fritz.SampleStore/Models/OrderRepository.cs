using System;
using System.Collections.Generic;

namespace Fritz.SampleStore.Models
{
  public class OrderRepository : BaseRepository<Order>
  {

    static OrderRepository()
    {

      _Items.Add(1, new Order { Id = 1, CustomerName = "Rey Bango", ShipToAddress = "Miami, FL", LineItems = new Dictionary<int, int> { { 1, 1 }, { 2, 1 } } });
      _Items.Add(2, new Order { Id = 2, CustomerName = "Gabe Sumner", ShipToAddress = "Palo Alto, CA", LineItems = new Dictionary<int, int> { { 2, 1 } } });
      _Items.Add(3, new Order { Id = 3, CustomerName = "Burke Holland", ShipToAddress = "Austin, TX", LineItems = new Dictionary<int, int> { { 3, 1 }} });

    }

  }

}