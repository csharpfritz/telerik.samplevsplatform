namespace Fritz.SampleStore.Models
{
  public interface IHasId
  {
    int Id { get; set; }
  }

}