using System;
using System.Collections.Generic;

namespace Fritz.SampleStore.Models
{
  public class BoardGameRepository : BaseRepository<BoardGame>
  {

    static BoardGameRepository()
    {

      _Items.Add(1, new BoardGame { Id = 1, Name = "Checkers", StockLocation = "A", Price = 7.99M, NumInStock = 10 });
      _Items.Add(2, new BoardGame { Id = 2, Name = "Chess", StockLocation = "B", Price = 12.99M, NumInStock = 20 });
      _Items.Add(3, new BoardGame { Id = 3, Name = "Backgammon", StockLocation = "C", Price = 15.99M, NumInStock = 5 });
      _Items.Add(4, new BoardGame { Id = 4, Name = "Parcheesi", StockLocation = "D", Price = 8.99M, NumInStock = 15 });

    }

  }

}