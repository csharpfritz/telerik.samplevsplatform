using System;
using System.Collections.Generic;
using System.Linq;

namespace Fritz.SampleStore.Models
{
  public class Order : IHasId
  {

    public Order()
    {
      ShipDate = DateTime.MaxValue;
    }

    public int Id { get; set; }

    public string CustomerName { get; set; }

    public string ShipToAddress { get; set; }

    public void AddLineItem(int gameId, int qty)
    {
      if (LineItems.Any(k => k.Key == gameId))
      {
        LineItems[gameId] += qty;
      }
      else
      {
        LineItems.Add(gameId, qty);
      }
    }

    public Dictionary<int,int> LineItems { get; set; }

    public List<Tuple<BoardGame, int>> GamesOrdered
    {
      get
      {
        var repo = new BoardGameRepository();
        return LineItems.Select(i => new Tuple<BoardGame,int>(repo.GetById(i.Key), i.Value)).ToList();
      }
    }

    public DateTime ShipDate { get; set; }

  }

}