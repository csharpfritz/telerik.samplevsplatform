using System;
using System.Collections.Generic;
using System.Linq;

namespace Fritz.SampleStore.Models
{
  public abstract class BaseRepository<T> where T : IHasId
  {

    protected static readonly Dictionary<int, T> _Items = new Dictionary<int, T>();

    public T GetById(int id)
    {
      return _Items[id];
    }

    public IEnumerable<T> Get(Func<T, bool> where)
    {
      return _Items.Values.Where(where);
    }

    public void Update(T item)
    {
      _Items[item.Id] = item;
    }

    public void Add(T item)
    {

      item.Id = _Items.Keys.Max() + 1;
      _Items[item.Id] = item;

    }

    public void Delete(int id)
    {
      _Items.Remove(id);
    }

  }

}