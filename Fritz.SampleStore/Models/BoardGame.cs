﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fritz.SampleStore.Models
{
  public class BoardGame : IHasId
  {

    public int Id { get; set; }

    public string Name { get; set; }

    public string StockLocation { get; set; }

    public decimal Price { get; set; }

    public int NumInStock { get; set; }

  }

}