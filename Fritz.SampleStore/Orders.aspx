﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="Fritz.SampleStore.Orders" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">

  <telerik:RadGrid runat="server" SelectMethod="GetItems" OnItemDataBound="Unnamed_ItemDataBound">
    <MasterTableView AutoGenerateColumns="false" DataKeyNames="Id" ItemType="Fritz.SampleStore.Models.Order">
      <Columns>
        <telerik:GridBoundColumn DataField="CustomerName" HeaderText="Customer Name" ItemStyle-VerticalAlign="Top"></telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ShipToAddress" HeaderText="Ship To" ItemStyle-VerticalAlign="Top"></telerik:GridBoundColumn>
        <telerik:GridTemplateColumn UniqueName="Items" HeaderText="Items">
          <ItemTemplate>
            <asp:Table runat="server" ID="itemTable" CssClass="itemsTable">

            </asp:Table>
          </ItemTemplate>
        </telerik:GridTemplateColumn>
        <telerik:GridTemplateColumn DataField="ShipDate" HeaderText="Shipped" ItemStyle-VerticalAlign="Top">
          <ItemTemplate>
            <%#: Item.ShipDate > DateTime.Now ? "-" : Item.ShipDate.ToShortDateString() %>
          </ItemTemplate>
        </telerik:GridTemplateColumn>
      </Columns>
    </MasterTableView>
  </telerik:RadGrid>


</asp:Content>