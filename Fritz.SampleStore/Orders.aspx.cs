﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fritz.SampleStore.Models;
using Telerik.Web.UI;

namespace Fritz.SampleStore
{
  public partial class Orders : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public IEnumerable<Order> GetItems()
    {
      var repo = new OrderRepository();
      return repo.Get(_ => true);
    }

    protected void Unnamed_ItemDataBound(object sender, GridItemEventArgs e)
    {

      GridDataItem item = e.Item as GridDataItem;
      if (item != null)
      {

        var thisOrder = item.DataItem as Order;
        var lineItems = thisOrder.GamesOrdered;

        var tbl = item.FindControl("itemTable") as Table;
        if (tbl != null)
        {

          foreach (var lineItem in lineItems)
          {
            var productCell = new TableCell();
            productCell.Text = string.Format("{0}: ({1})", lineItem.Item1.Name, lineItem.Item2);

            var newRow = new TableRow();
            newRow.Cells.Add(productCell);
            tbl.Rows.Add(newRow);
          }
        }

      }

    }

  }
}